package hekate

import (
	"errors"
	"net"
	"os"
	"sync"
	"sync/atomic"
	"time"
)

// Conn is a connection-based wrapper around a UDP connection.
//
// It provides the handling of lost packages, RTT measurement and disconnect detection.
// Optionally it provides functionality to keep the connection alive.
type Conn struct {
	conn       *net.UDPConn
	addr       *net.UDPAddr
	protocolId uint32

	localSequence       uint32
	waitingForAck       map[uint32]Package
	timeSend            map[uint32]time.Time
	timeSendLastMessage time.Time

	remainingAcks       []uint32
	remoteSequence      uint32
	lastMessageReceived time.Time

	LostTimeout       time.Duration // LostTimeout is the duration after which a package is considered lost and must be resend
	KeepAliveInterval time.Duration // KeepAliveInterval is the duration after which a message must be send to the client to keep the connection alive
	DisconnectTimeout time.Duration // DisconnectTimeout is the duration after which a client is considered as disconnected if no package arrives

	rtt time.Duration

	waitingLock  sync.RWMutex
	timeSendLock sync.RWMutex
}

// Wrap returns a hekate.Conn based on the given UDP connection.
//
// The given address determines the sending and receiving address. Incoming packages from another address are dropped.
// When sending data, the given address is used.
//
// The protocolId is used in outgoing messages and incoming messages are checked, if they contain the protocolId. If they don't contain it, they are dropped.
func Wrap(conn *net.UDPConn, addr *net.UDPAddr, protocolId uint32) *Conn {
	return &Conn{
		conn:       conn,
		addr:       addr,
		protocolId: protocolId,

		waitingForAck: make(map[uint32]Package),
		timeSend:      make(map[uint32]time.Time),

		LostTimeout:       time.Second,
		KeepAliveInterval: time.Second / 2,
		DisconnectTimeout: time.Second,
	}
}

func (c *Conn) ProtocolID() uint32 {
	return c.protocolId
}

func (c *Conn) RTT() time.Duration {
	return c.rtt
}

// Disconnected checks if the peer has disconnected.
//
// See DisconnectTimeout for the disconnection condition
func (c *Conn) Disconnected() bool {
	return time.Since(c.lastMessageReceived) > c.DisconnectTimeout
}

// KeepAlive checks if a package has been send within the KeepAliveInterval
// and sends an empty package if the KeepAliveInterval has been exceeded
func (c *Conn) KeepAlive() {
	if time.Since(c.timeSendLastMessage) >= c.KeepAliveInterval {
		_, _ = c.Write([]byte{})
	}
}

func (c *Conn) getWaitingForAck(ack uint32) Package {
	c.waitingLock.RLock()
	defer c.waitingLock.RUnlock()
	return c.waitingForAck[ack]
}

func (c *Conn) setWaitingForAck(p Package) {
	c.waitingLock.Lock()
	defer c.waitingLock.Unlock()
	c.waitingForAck[p.Sequence] = p
}

func (c *Conn) removeWaitingForAck(ack uint32) {
	c.waitingLock.Lock()
	defer c.waitingLock.Unlock()
	delete(c.waitingForAck, ack)
}

func (c *Conn) getTimeSend(ack uint32) (time.Time, bool) {
	c.timeSendLock.RLock()
	defer c.timeSendLock.RUnlock()
	t, ok := c.timeSend[ack]
	return t, ok
}

func (c *Conn) setTimeSend(seq uint32, t time.Time) {
	c.timeSendLock.Lock()
	defer c.timeSendLock.Unlock()
	c.timeSend[seq] = t
}

func (c *Conn) removeTimeSend(ack uint32) {
	c.timeSendLock.Lock()
	defer c.timeSendLock.Unlock()
	delete(c.timeSend, ack)
}

func (c *Conn) iteratorTimeSend() (seqs []uint32, times []time.Time) {
	c.timeSendLock.RLock()
	defer c.timeSendLock.RUnlock()

	seqs = make([]uint32, len(c.timeSend))
	times = make([]time.Time, len(c.timeSend))

	i := 0
	for seq, t := range c.timeSend {
		seqs[i] = seq
		times[i] = t
		i++
	}
	return

}

// ResendLostPackages checks if packages are lost and attempts to resend them.
//
// LostTimeout is used to check if a package is lost
func (c *Conn) ResendLostPackages() {
	seqs, times := c.iteratorTimeSend()
	for i := 0; i < len(seqs); i++ {
		seq := seqs[i]
		t := times[i]
		if time.Since(t) > c.LostTimeout {
			_, err := c.send(c.getWaitingForAck(seq))
			if err != nil {
				if !errors.Is(err, os.ErrDeadlineExceeded) {
					// TODO: Unknown error
				}
			}

			// The package is considered as lost
			// We don't need to keep track of it
			c.removeTimeSend(seq)
			c.removeWaitingForAck(seq)

		}
	}
}

func (c *Conn) nextSequence() uint32 {
	return atomic.AddUint32(&c.localSequence, 1)
}

func (c *Conn) pushAck(ack uint32) {
	// we are keeping only 32 acks, since Package.PrevAcks is a uint32
	reduceBy := 32
	current := len(c.remainingAcks)
	if current < reduceBy {
		reduceBy = current
	}
	c.remainingAcks = append(c.remainingAcks[current-reduceBy:], ack)
}

func (c *Conn) confirmSequence(seq uint32) {
	c.removeTimeSend(seq)
	c.removeWaitingForAck(seq)
}

func (c *Conn) updateRTT(seq uint32) {
	t, ok := c.getTimeSend(seq)
	if !ok {
		return
	}
	d := time.Since(t)
	c.rtt += time.Duration(float32(c.rtt)*0.9 + float32(d)*0.1)
}

func (c *Conn) handleRead(i int, data, b []byte) (int, error) {
	if i < sizeHeader {
		return 0, ErrBrokenHeader{}
	}

	p := retrievePackage(data)

	if p.ProtocolId != c.ProtocolID() {
		return 0, ErrUnknownProtocol{}
	}

	c.pushAck(p.Sequence)
	if c.remoteSequence < p.Sequence {
		c.remoteSequence = p.Sequence
	}

	c.updateRTT(p.Ack)
	c.confirmSequence(p.Ack)
	for _, ack := range p.RestoreAcks() {
		c.updateRTT(ack)
		c.confirmSequence(ack)
	}

	c.lastMessageReceived = time.Now()

	return copy(b, p.Data), nil
}

// Implements net.Conn
func (c *Conn) Write(b []byte) (int, error) {
	p := Package{
		ProtocolId: c.protocolId,
		Ack:        c.remoteSequence,
		Data:       b,
	}
	p.StoreAcks(c.remainingAcks)
	return c.send(p)
}

func (c *Conn) send(p Package) (int, error) {
	p.Sequence = c.nextSequence()
	c.setWaitingForAck(p)

	i, err := c.conn.WriteToUDP(p.Bytes(), c.addr)
	t := time.Now()
	c.setTimeSend(p.Sequence, t)
	c.timeSendLastMessage = t
	return i - sizeHeader, err

}

// Implements net.Conn
// Reads a package if it is send by the connected client, otherwise it will be dropped
//
// Attention: Should not be used if the connections are handled by Server as the underlying connection is shared!
func (c *Conn) Read(b []byte) (int, error) {
	d := make([]byte, len(b)+sizeHeader)
	i, addr, err := c.conn.ReadFromUDP(d)

	if addr.String() != c.addr.String() {
		return 0, ErrUnknownClient{}
	}

	if err != nil {
		return 0, err
	}

	return c.handleRead(i, d, b)
}

// Implements net.Conn
func (c *Conn) LocalAddr() net.Addr {
	return c.conn.LocalAddr()
}

// Implements net.Conn
func (c *Conn) RemoteAddr() net.Addr {
	return c.addr
}

// Implements net.Conn
// Since the server and the connection share
func (c *Conn) SetDeadline(t time.Time) error {
	return c.conn.SetDeadline(t)
}

// Implements net.Conn
func (c *Conn) SetReadDeadline(t time.Time) error {
	return c.conn.SetReadDeadline(t)
}

// Implements net.Conn
func (c *Conn) SetWriteDeadline(t time.Time) error {
	return c.conn.SetWriteDeadline(t)
}

// Implements net.Conn
//
// Attention: Should not be used if the connections are handled by Server as the underlying connection is shared!
func (c *Conn) Close() error {
	return c.conn.Close()
}

// Server contains all connections and creates new connections from incoming packages.
type Server struct {
	conn       *net.UDPConn
	conns      map[string]*Conn
	protocolId uint32
}

// Listen creates a server which will listen on the given address.
//
// The protocolId is used to distinguish potential clients from random input.
//
// See net.ListenUDP for further details.
func Listen(network, address string, protocolId uint32) (*Server, error) {
	addr, err := net.ResolveUDPAddr(network, address)
	if err != nil {
		return nil, err
	}
	udp, err := net.ListenUDP(network, addr)
	if err != nil {
		return nil, err
	}

	s := &Server{
		conn:       udp,
		conns:      make(map[string]*Conn),
		protocolId: protocolId,
	}
	return s, nil
}

func (s *Server) ProtocolID() uint32 {
	return s.protocolId
}

// ReadFromHekate acts like the net.PacketConn ReadFrom method but returns a hekate.Conn
//
// As long as the client is connected it will return the same Conn for this client.
func (s *Server) ReadFromHekate(b []byte) (int, *Conn, error) {
	d := make([]byte, len(b)+sizeHeader)
	i, addr, err := s.conn.ReadFromUDP(d)

	if err != nil {
		return 0, nil, err
	}

	if _, ok := s.conns[addr.String()]; !ok {
		conn := Wrap(s.conn, addr, s.protocolId)
		s.conns[addr.String()] = conn
	}
	conn := s.conns[addr.String()]

	j, err := conn.handleRead(i, d, b)
	if err != nil {
		return 0, nil, err
	}

	return j, conn, err
}

// ResendLostPackages attempts to resend the lost packages of all connections
//
// See Conn.ResendLostPackages for details
func (s *Server) ResendLostPackages() {
	for _, c := range s.conns {
		c.ResendLostPackages()
	}
}

// KeepAlive keeps all connections alive
//
// See Conn.KeepAlive for details
func (s *Server) KeepAlive() {
	for _, c := range s.conns {
		c.KeepAlive()
	}
}

// CleanUpDisconnects checks all connections if their client has disconnected.
// If a disconnected is found, it will be removed.
// Returns a slice of all disconnected clients.
//
// See Conn.Disconnected for details
func (s *Server) CleanUpDisconnects() (conns []*Conn) {
	for k, c := range s.conns {
		if c.Disconnected() {
			conns = append(conns, c)
			delete(s.conns, k)
		}

	}
	return conns

}

func (s *Server) Close() error {
	return s.conn.Close()
}

func (s *Server) SetReadDeadline(t time.Time) error {
	return s.conn.SetReadDeadline(t)
}

var _ net.Conn = (*Conn)(nil)
