/*

Package hekate provides a wrapper for reliable UDP connections.
It provides functionality to resend lost packages, detect disconnects and measure the RTT.

The Listen function creates servers:
  s, err := hekate.Listen("udp", ":8080", 0xF001)
  if err != nil {
    // handle error
  }
  for {
    s.ResendLostPackages()
    s.KeepAlive()

    b := make([]byte, 1024)
    _, clientConn, err := s.ReadFromHekate(b)
    if err != nil {
      // handle error
    }
    doSomething(b, clientConn)

    disconnects := s.CleanUpDisconnects()
    cleanUp(disconnects)
  }


*/
package hekate
