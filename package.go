package hekate

import (
	"encoding/binary"
)

const (
	sizeProtocolId = 4
	sizeSequence   = 4
	sizeAck        = 4
	sizePrevAcks   = 4
	sizeHeader     = sizeProtocolId + sizeSequence + sizeAck + sizePrevAcks

	endProtocolId = startProtocolId + sizeProtocolId
	endSequence   = startSequence + sizeSequence
	endAck        = startAck + sizeAck
	endPrevAcks   = startPrevAcks + sizePrevAcks

	startProtocolId = 0
	startSequence   = endProtocolId
	startAck        = endSequence
	startPrevAcks   = endAck
	startData       = endPrevAcks
)

// Package contains the header and the payload being send by hekate.
type Package struct {
	ProtocolId uint32
	Sequence   uint32
	Ack        uint32
	PrevAcks   uint32
	Data       []byte
}

func retrievePackage(b []byte) (p Package) {
	p.ProtocolId = binary.LittleEndian.Uint32(b[startProtocolId:endProtocolId])
	p.Sequence = binary.LittleEndian.Uint32(b[startSequence:endSequence])
	p.Ack = binary.LittleEndian.Uint32(b[startAck:endAck])
	p.PrevAcks = binary.LittleEndian.Uint32(b[startPrevAcks:endPrevAcks])
	p.Data = b[startData:]
	return
}

func (p Package) Bytes() (b []byte) {
	b = make([]byte, sizeHeader)
	binary.LittleEndian.PutUint32(b[startProtocolId:], p.ProtocolId)
	binary.LittleEndian.PutUint32(b[startSequence:], p.Sequence)
	binary.LittleEndian.PutUint32(b[startAck:], p.Ack)
	binary.LittleEndian.PutUint32(b[startPrevAcks:], p.PrevAcks)
	b = append(b, p.Data...)

	return
}

func (p Package) RestoreAcks() (acks []uint32) {
	for i := uint32(0); i < 32; i++ {
		if (p.PrevAcks>>i)&1 == 1 {
			acks = append(acks, p.Ack-i)
		}
	}
	return
}

func (p *Package) StoreAcks(acks []uint32) {
	for _, ack := range acks {
		z := p.Ack - ack
		p.PrevAcks |= 1 << z
	}
}
