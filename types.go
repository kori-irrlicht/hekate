package hekate

type ErrBrokenHeader struct {
}

func (e ErrBrokenHeader) Error() string {
	return "invalid header"
}

type ErrUnknownProtocol struct {
}

func (e ErrUnknownProtocol) Error() string {
	return "unknown protocol"
}

type ErrUnknownClient struct{}

func (e ErrUnknownClient) Error() string {
	return "unknown client"
}
